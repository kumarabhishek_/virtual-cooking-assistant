
## Virtual Cooking Assistant
*Text and voice based virtual assistant to simplify peoples� lives in a way that makes cooking at home easier, fun and delightful.*

### Purpose:
Have you ever wanted to cook something but had hard-time finding a recipe online. Have you ever wondered if you had an assistant you could guide you step by step through the cooking process ? 

### Solution:
1. An AI based chatbot that can also speak to you
   
2. Searches for a recipe as per your query
   
3. Can recommend you a random recipe if you are unsure what to eat
   
4. Reads aloud each cooking step and waits for you to finish the step before proceeding further.

### Technology stack:
1. Rasa Open source
2. Web-speech API
3. React based chatroom component by [ScalableMinds](https://github.com/scalableminds/chatroom)
4. Apache web-server
5. [Spoonacular API](https://spoonacular.com/food-api) for recipe querying
---

## Installation

    pip3 install rasa==2.6.3

## Usage

Clone the repository. 

Start the Rasa action server type:

    rasa run actions

Next, start the Rasa server:

    rasa run --credentials ./credentials.yml  --enable-api --auth-token XYZ123 --model ./models --endpoints ./endpoints.yml --cors "*"

Open this file in the Chromium-based browser:

    virtual-cooking-assistant/chatroom/index.html

Note:
If the browser blocks access to microphone, use this guide: https://stackoverflow.com/questions/52759992/how-to-access-camera-and-microphone-in-chrome-without-https

## Live demo
[https://34.138.189.246/](https://34.138.189.246/)
