# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction
from typing import Any, Text, Dict, List
from spoonacular_client import spoonacular_client


# Action to fetch a recipe based on the user's query
class ActionListOfRecipes(Action):

    def name(self) -> Text:
        return "action_list_of_recipes"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        food_name = tracker.get_slot("food")

        recipe_find = spoonacular_client.fetch_recipes(food_name)
        if recipe_find != 0:
            recipe_id = recipe_find['results'][0]["id"]

            dispatcher.utter_message(text="I have found a recipe with the title " + recipe_find['results'][0][
                'title'] + " for your query {}. Would you like to continue?".format(food_name))
            try:
                dispatcher.utter_message(image="" + recipe_find['results'][0]['image'])
            except:
                pass

            return [SlotSet("recipe_id", recipe_id)]

        else:
            dispatcher.utter_message(text="Could not find any recipe. Please try again.")
            return [FollowupAction('action_restart')]


# After the user selects a recipe, this action is used to fetch the detailed recipe instructions
class ActionDetailedRecipe(Action):

    def name(self) -> Text:
        return "action_get_detailed_recipe_instructions"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        recipe_id = tracker.get_slot("recipe_id")
        detailed_instructions = []

        # Handling error in case of API failure
        if spoonacular_client.get_recipe_instructions(recipe_id) == 404:
            dispatcher.utter_message(text="Something is wrong. Cannot proceed further with your request! Sorry!")
            return [FollowupAction("action_restart")]

        recipe_instructions_with_ingredients_list = spoonacular_client.get_recipe_instructions(recipe_id)
        recipe_instructions = recipe_instructions_with_ingredients_list['steps']
        ingredient_list = recipe_instructions_with_ingredients_list['ingredients']
        dispatcher.utter_message(text="Here are the ingredients: ")
        for item in ingredient_list:
            dispatcher.utter_message(text="" + item)
        dispatcher.utter_message(text="Now, let's start the cooking process: ")

        for eachStep in recipe_instructions:
            detailed_instructions.append(eachStep['step'])

        return [SlotSet("recipe_instructions", recipe_instructions), FollowupAction("action_recipe_each_step"),
                SlotSet("recipe_step", 0)]


# Follow-up Action for step by step process for the recipe selected. Called each time the user says 'Next step'
class ActionRecipeEachStep(Action):

    def name(self) -> Text:
        return "action_recipe_each_step"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # If the slots are not set then restarting conversation
        if tracker.get_slot("recipe_instructions") is None:
            dispatcher.utter_message(text="Unfortunately you haven't selected any recipe item to proceed further. "
                                          "Please say something like \'I want to eat pizza\'")
            return [FollowupAction("action_restart")]

        # Printing each step process of the selected recipe
        recipe_steps = tracker.get_slot("recipe_instructions")
        step_number = tracker.get_slot("recipe_step")
        dispatcher.utter_message(text="" + recipe_steps[step_number]['step'])
        step_number = step_number + 1

        # If it's last step then restart the conversation and delete all slots
        if step_number >= len(recipe_steps):
            dispatcher.utter_message(text="That was the last step. Enjoy your meal.")
            return [FollowupAction("action_restart")]

        return [SlotSet("recipe_step", step_number)]


# Action to decrease the step number for going to previous step
class ActionRecipeGoPrevStep(Action):

    def name(self) -> Text:
        return "action_go_prev_step"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # If the slots are not set then restarting conversation
        if tracker.get_slot("recipe_instructions") is None:
            dispatcher.utter_message(text="Unfortunately you haven't selected any recipe item to proceed further. "
                                          "Please say something like \'I want to eat pizza\'")
            return [FollowupAction("action_restart")]

        step_number = tracker.get_slot("recipe_step")
        step_number = step_number - 1
        return [SlotSet("recipe_step", step_number), FollowupAction("action_recipe_each_step")]


# Action to find a random recipe(when user doesn't know what to eat)
class ActionFindRandomRecipe(Action):

    def name(self) -> Text:
        return "action_find_random_recipe"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if spoonacular_client.get_random_recipe() == 404:
            dispatcher.utter_message(text="Something broke! Sorry, try again.")
            return [FollowupAction('action_restart')]

        random_recipe_item = spoonacular_client.get_random_recipe()
        if random_recipe_item['title']:
            ingredients_list = ""
            recipe_id = random_recipe_item['id']
            dispatcher.utter_message(text="Found a dish with the title " + random_recipe_item[
                'title'] + ". To prepare this, you would need these ingredients:")
            for eachIngredient in random_recipe_item['extendedIngredients']:
                ingredients_list = ingredients_list + str(eachIngredient['name'] + ", ")
            dispatcher.utter_message(text="" + ingredients_list + ". Would you like to continue ?")

            try:
                dispatcher.utter_message(image="" + random_recipe_item['image'])
            except:
                pass
            return [SlotSet("recipe_id", recipe_id)]


# Action to find similar recipe as the first suggestion as per the user's query
class ActionFindSimilarRecipe(Action):

    def name(self) -> Text:
        return "action_get_similar_recipe"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        recipe_id = tracker.get_slot("recipe_id")
        similar_recipe_item = spoonacular_client.get_similar_recipe(recipe_id)

        if similar_recipe_item != "error" | similar_recipe_item is not None:
            new_recipe_id = similar_recipe_item["id"]

            dispatcher.utter_message(
                text="No problem! I have found another recipe with the title " + similar_recipe_item[
                    'title'] + ". Would you like to continue?")
            return [SlotSet("recipe_id", new_recipe_id)]
        else:
            dispatcher.utter_message(text="Could not find any recipe. Please try again.")
            return [FollowupAction('action_restart')]
