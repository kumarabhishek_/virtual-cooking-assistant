import requests
import json

# Spoonacular credentials
url = 'https://api.spoonacular.com/recipes/'
apiKey = '3bba5a8242d84002858b08788f978c9e'


# Fetches recipe matching the search query
def fetch_recipes(query='chicken%20tikka'):
    number = '2'
    apiEndPoint = 'complexSearch'
    query.replace(" ", "%20")
    response = requests.get(url + apiEndPoint + '?query=' + query + '&number=' + number + '&apiKey=' + apiKey)

    if response.json()['results']:
        recipe_info = response.json()
        # print(recipe_info['results'][0]["id"])
        return recipe_info
    else:
        return 0


# Fetches the list of ingredients required for the selected recipe
def get_recipe_ingredients(recipe_id='638389'):
    ingredients_list = []
    apiEndPoint = 'ingredientWidget.json'
    response = requests.get(url + str(recipe_id) + '/' + apiEndPoint + '?apiKey=' + apiKey)
    for eachItem in response.json()['ingredients']:
        ingredients_list.append(eachItem['name'])
    return ingredients_list


# Fetches the detailed instructions of the selected recipe
def get_recipe_instructions(recipe_id='638389'):
    apiEndPoint = 'analyzedInstructions'
    response = requests.get(url + str(recipe_id) + '/' + apiEndPoint + '?apiKey=' + apiKey)

    ingredient_list = get_recipe_ingredients(recipe_id)
    if response.status_code == 200:
        if response.json()[0]['steps']:
            result = {'ingredients': ingredient_list, 'steps': response.json()[0]['steps']}
        else:
            return 404

        return result
    else:
        print("error")


# Fetches a random recipe
def get_random_recipe():
    apiEndPoint = 'random'
    response = requests.get(url + apiEndPoint + '?apiKey=' + apiKey)
    if response.status_code == 200:
        return response.json()['recipes'][0]
    else:
        return 404


# Gets a similar kind of recipe for user query
def get_similar_recipe(recipe_id='638389'):
    apiEndPoint = 'similar'
    response = requests.get(url + str(recipe_id) + '/' + apiEndPoint + '?apiKey=' + apiKey )
    if response.status_code == 200:
        if response.json()[0]['title']:
            return response.json()[0]
        else:
            return "error"
